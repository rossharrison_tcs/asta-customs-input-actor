package com.postnord.asta.processor;

import java.util.Properties;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KeyValueMapper;
import org.apache.kafka.streams.kstream.Predicate;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.ValueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.postnord.asta.constant.CustomConstant;
import com.postnord.asta.model.Event;
import com.postnord.asta.util.EventToTransactionMapper;
import com.postnord.asta.util.KafkaUtil;
import com.postnord.asta.util.StringToEventMapper;
import com.postnord.asta.util.SystemUtil;
import com.postnord.asta.util.TransactionToJsonStringMapper;
import com.postnord.potapi.model.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Processor {

	private static final Logger LOG = LoggerFactory.getLogger(Processor.class);

	private KafkaStreams kafkaStreams;

	public void process() {
		LOG.debug("Start of process()");
		kafkaStreams = new KafkaStreams(topology(), config());
		kafkaStreams.start();
		LOG.debug("End of process()");
	}

	private Properties config() {
		LOG.debug("Start of config()");
		return KafkaUtil.getStreamsApplicationProperties(CustomConstant.APPLICATION_ID);
	}

	public Topology topology() {

		LOG.debug("Start of topology()");
		StreamsBuilder builder = new StreamsBuilder();

		KStream<String, String> inputStream = builder
                .stream(inputTopic(), consumeEventResult());

		inputStream
                .filter(isNull())
                .to(newOutputTopic(), produceOutput());

		KStream<String, Transaction> outputStream = inputStream
                .filter(isNull())
                .mapValues(stringToEventMapper())
				.mapValues(eventToPotapi());

		outputStream
                .map(transactionToJsonString())
                .to(outputTopic(), produceTransactionString());

		return builder
                .build();
	}

	private KeyValueMapper<String, Transaction, KeyValue<String, String>> transactionToJsonString() {
		return new TransactionToJsonStringMapper();
	}

	private ValueMapper<String, Event> stringToEventMapper() {
		return new StringToEventMapper();
	}

	private ValueMapper<Event, Transaction> eventToPotapi() {
		return new EventToTransactionMapper();
	}

	private Consumed<String, String> consumeEventResult() {

		Serde<String> keySerde = Serdes.String();
		Serde<String> valueSerde = Serdes.String();

		return Consumed.with(keySerde, valueSerde);

	}

	private String inputTopic() {
		return SystemUtil.getEnvironmentVariable("INPUT_TOPIC", "prod-asta-se-event");
	}

	private Predicate<String, String> isNull() {
		return (k, v) -> v != null;
	}

	private Produced<String, String> produceTransactionString() {
		Serde<String> keySerde = Serdes.String();
		Serde<String> valueSerde = Serdes.String();
		return Produced.with(keySerde, valueSerde);
	}

	private Produced<String, String> produceOutput() {
		Serde<String> keySerde = Serdes.String();
		Serde<String> valueSerde = Serdes.String();

        LOG.debug("string to firstscan :" + valueSerde);
		return Produced.with(keySerde, valueSerde);
	}

	private String outputTopic() {
		return SystemUtil.getEnvironmentVariable("OUTPUT_TOPIC", CustomConstant.POTAPI_TRANSACTION_READ);
	}

	private String newOutputTopic() {
		return SystemUtil.getEnvironmentVariable("FIRST_SCAN_OUTPUT_TOPIC",
				CustomConstant.PROD_ASTA_SE_FIRSTSCAN);
	}


}
