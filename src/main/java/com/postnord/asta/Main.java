package com.postnord.asta;
import com.postnord.potapi.version.VersionHttpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.postnord.asta.processor.Processor;

public class Main {

	private static VersionHttpServer versionServer;

	private static final Logger LOG = LoggerFactory.getLogger(Main.class);
	
	public static void main(String[] args) {
		int port = getPort();
		startVersionServer(port);

		LOG.debug("Start of Main");
		Processor processor = new Processor();
		processor.process();
		LOG.debug("End of Main");
	}

	private static int getPort(String... args) {
		int defaultPort = 8080;
		if (args.length > 0) {
			String port = args[0];
			try {
				return Integer.parseInt(port);
			} catch (NumberFormatException e) {
				return defaultPort;
			}
		}

		return defaultPort;
	}

	private static void startVersionServer(int port) {
		versionServer = new VersionHttpServer(port);
		versionServer.start();
	}

	static void shutdown() {
		versionServer.stop();
	}

}
