package com.postnord.asta.util;

import com.postnord.asta.constant.ImpostCategory;
import com.postnord.potapi.model.types.Customs;
import com.postnord.potapi.model.types.CustomsOriginal;
import org.apache.kafka.streams.kstream.ValueMapper;

import com.postnord.asta.constant.CustomConstant;
import com.postnord.asta.model.Event;
import com.postnord.potapi.model.Item;
import com.postnord.potapi.model.Payload;
import com.postnord.potapi.model.Transaction;
import com.postnord.potapi.model.Transaction.Builder;
import java.util.UUID;


public class EventToTransactionMapper implements ValueMapper<Event, Transaction> {

	@Override
	public Transaction apply(Event event) {
		Transaction.Builder builder = new Builder();

		UUID transactionId = UUID.randomUUID();

		return builder.withId(transactionId.toString())
				.withEventTime(getInputDateProcessed(event.geteventTime()))
				.withRetries(0)
				.withAction(CustomConstant.WRITE)
				.withPayload(buildPayload(event))
                .withActor(CustomConstant.ASTA)
				.build();

	}

	private Payload buildPayload(Event event) {
		Payload.Builder builder = new com.postnord.potapi.model.Payload.Builder();
		return builder.withItem(buildItem(event)).build();

	}

	private Item buildItem(Event event) {
		Item.Builder builder = new com.postnord.potapi.model.Item.Builder();
		return builder
                .withItemId(UUID.nameUUIDFromBytes(event.getPnItemId().getBytes()).toString())
                .withPnItemId(event.getPnItemId())
				.withProcessingTime(getInputDateProcessed(event.geteventTime()))
				.withProductCode(event.getProductCode() == null ? null : event.getProductCode())
				.withSource(event.getSource() == null ? null : event.getSource())
				.withCustoms(buildCustoms(event))
				.build();
	}

	private Customs buildCustoms(Event event) {
		Customs.Builder builder = new Customs.Builder();
		return builder.withOriginal(buildOriginals(event)).build();
	}

	private CustomsOriginal buildOriginals(Event event){
		CustomsOriginal.Builder builder = new CustomsOriginal.Builder();
		String pmImpostCategory ;
		if(event != null && event.getEuFlag().equalsIgnoreCase("0")){
			pmImpostCategory = ImpostCategory.NON_EU.toString();
		}else if(event != null && event.getEuFlag().equalsIgnoreCase("1")){
			pmImpostCategory = ImpostCategory.EU.toString();
		}else{
			pmImpostCategory = ImpostCategory.UNKNOWN.toString();
		}
		return builder.withPnImportCategory(pmImpostCategory).build();
	}

    public String getInputDateProcessed(String date) {

        if (date == null) {
            return null;
        } else if (date.contains(CustomConstant.T) && date.contains(CustomConstant.PLUS)
                && date.contains(CustomConstant.Z)) {
            int startIndex = date.indexOf(CustomConstant.PLUS);
            date = date.substring(0, startIndex) + CustomConstant.Z;
        } else if (date.contains(CustomConstant.T) && date.contains(CustomConstant.PLUS)) {
            int startIndex = date.indexOf(CustomConstant.PLUS);
            date = date.substring(0, startIndex) + CustomConstant.Z;
        }else if(date.contains(CustomConstant.T) && date.contains(CustomConstant.Z)){
            //do nothing.
        }
        else if (date.contains(CustomConstant.T)) {
            date = date + CustomConstant.Z;
        }

        return date;

    }
}
