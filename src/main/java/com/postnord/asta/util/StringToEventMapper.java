package com.postnord.asta.util;

import java.io.IOException;

import org.apache.kafka.streams.kstream.ValueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.postnord.asta.model.Event;

public class StringToEventMapper implements ValueMapper<String, Event> {

	private static final Logger LOG = LoggerFactory.getLogger(StringToEventMapper.class);

	@Override
	public Event apply(String message) {
		ObjectMapper mapper = new ObjectMapper();
		Event event = new Event();
		try {
			event = mapper.readValue(message, Event.class);
			LOG.debug("EU flag : " + event.getEuFlag());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return event;
	}

}
