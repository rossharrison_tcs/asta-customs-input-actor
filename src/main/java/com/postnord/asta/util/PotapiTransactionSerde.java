package com.postnord.asta.util;

import com.postnord.potapi.model.Transaction;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

import java.nio.charset.StandardCharsets;
import java.util.Map;

public class PotapiTransactionSerde implements Serde<Transaction> {
    @Override
    public void configure(Map<String, ?> map, boolean b) {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public void close() {
    }

    @Override
    public Serializer<Transaction> serializer() {
        return new TransactionSerializer();
    }

    @Override
    public Deserializer<Transaction> deserializer() {
        return new TransactionDeserializer();
    }

    private class TransactionSerializer implements Serializer<Transaction> {
        @Override
        public void configure(Map<String, ?> map, boolean b) {
        }

        @Override
        public byte[] serialize(String topic, Transaction transaction) {
            return transaction.toJson().getBytes(StandardCharsets.UTF_8);
        }

        @Override
        public void close() {
        }
    }

    private class TransactionDeserializer implements Deserializer<Transaction> {
        @Override
        public void configure(Map<String, ?> map, boolean b) {
        }

        @Override
        public Transaction deserialize(String s, byte[] data) {
            String transaction = new String(data, StandardCharsets.UTF_8);
            return Transaction.fromJson(transaction);
        }

        @Override
        public void close() {
        }
    }
}