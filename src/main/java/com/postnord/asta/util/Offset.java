package com.postnord.asta.util;

public enum Offset {
    LATEST("latest"),
    EARLIEST("earliest"),
    NONE("none"),
    SMALLEST("smallest");

    private final String configurationValue;

    Offset(String configurationValue) {
        this.configurationValue = configurationValue;
    }

    public String value() {
        return configurationValue;
    }

    public static Offset fromString(String configurationValue) {
        for (Offset offset : Offset.values()) {
            if (offset.value().equals(configurationValue)) {
                return offset;
            }
        }
        return null;
    }
}
