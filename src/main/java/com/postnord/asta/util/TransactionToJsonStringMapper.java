package com.postnord.asta.util;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KeyValueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.postnord.potapi.model.Transaction;

public class TransactionToJsonStringMapper implements KeyValueMapper<String, Transaction, KeyValue<String, String>> {

	private static final Logger LOG = LoggerFactory.getLogger(TransactionToJsonStringMapper.class);

	@Override
	public KeyValue<String, String> apply(String k, Transaction value) {
		String key = value.getTransactionId() == null ? null : value.getTransactionId().getTransactionId();
		String transactionAsJsonString = value.toJson();
		LOG.debug("Output JSON Message :  " + transactionAsJsonString);
		return new KeyValue<String, String>(key, transactionAsJsonString);
	}
}
