package com.postnord.asta.constant;

public enum ImpostCategory {
    EU,
    NON_EU,
    UNKNOWN;

}
