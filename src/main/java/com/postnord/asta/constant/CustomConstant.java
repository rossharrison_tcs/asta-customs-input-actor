package com.postnord.asta.constant;

public class CustomConstant {

	public static final String APPLICATION_ID = "asta-customs-input-actor";

	public static final String EVENT = "event";

	public static final String AUTO_OFFSET_RESET = "earliest";

	public static final String ENABLE_AUTO_COMMIT = "false";

	public static final String CURRENCY = "currency";

	public static final String TOTAL_STATED_VALUE = "totalStatedValue";

    public static final String ASTA = "asta";

	public static final String UNIT = "unit";

	public static final String TOTAL_STATED_WEIGHT = "totalStatedWeight";

	public static final String CUSTOMS_LABEL = "customsLabel";

	public static final String CODED_ADDRESS = "codedAddress";

	public static final String CONSIGNEE = "consignee";

	public static final String CONSIGNOR = "consignor";

	public static final String ADDITIONAL_SERVICE = "additionalService";

	public static final String MEASUREMENT = "measurement";

	public static final String CODE = "code";

	public static final String SERVICE = "service";

	public static final String NAME = "name";

	public static final String LOCATION = "location";

	public static final String TYPE = "type";

	public static final String LOCAL_CODE = "localCode";

	public static final String SCHEMA_VERSION = "schemaVersion";

	public static final String USER_ID = "userId";

	public static final String DATE = "date";

	public static final String ID = "id";

	public static final String COUNTRY = "country";

	public static final String MOBILE = "mobile";

	public static final String PHONE = "phone";

	public static final String CITY = "city";

	public static final String POSTCODE = "postcode";

	public static final String STREET2 = "street2";

	public static final String STREET1 = "street1";

	public static final String STOPPOINT = "stopPoint";

	public static final String DELIVERYPOINT = "deliveryPoint";

	public static final String HOUSINGID = "housingId";

	public static final String ORIGINATING_COUNTRY = "originatingCountry";

	public static final String IS_COMMERCIAL_RECIPIENT = "isCommercialRecipient";

	public static final String IS_OTHER = "isOther";

	public static final String IS_COMMERCIAL_SAMPLE = "isCommercialSample";

	public static final String IS_DOCUMENT = "isDocument";

	public static final String IS_GIFT = "isGift";

	public static final String ITEM_DESCRIPTION = "itemDescription";

	public static final String AMOUNT = "amount";

	public static final String ACCOUNT = "account";

	public static final String WEIGHT = "weight";

	public static final String GLP = "GLP";

	public static final String WRITE = "write";

	public static final String POTAPI_TRANSACTION_READ = "potapi-transaction-read";

	public static final String DEV_BOOTSTRAP_SERVER = "kafka-1.orm-dev-service:9092,kafka-2.orm-dev-service:9092,kafka-3.orm-dev-service:9092";

	public static final String AT_BOOTSTRAP_SERVER = "kafka-1.orm-at-service:9092,kafka-2.orm-at-service:9092,kafka-3.orm-at-service:9092";

	public static final String PROD_BOOTSTRAP_SERVER = "kafka-1.orm-prod-service:9092,kafka-2.orm-prod-service:9092,kafka-3.orm-prod-service:9092";

	public static final String LOCAL_BOOTSTRAP_SERVER = "127.0.0.1:9092";
	
	//public static final String INPUT_TOPIC = "INPUT_TOPIC";
	
	//public static final String FIRST_OUTPUT_TOPIC = "FIRST_OUTPUT_TOPIC";
	
	public static final String DEV_ASTA_SE_EVENT = "dev-asta-se-event";
	
	public static final String AT_ASTA_SE_EVENT = "at-asta-se-event";
	
	public static final String PROD_ASTA_SE_EVENT = "prod-asta-se-event";
	
	public static final String DEV_ASTA_SE_FIRSTSCAN = "dev-asta-se-firstscan";
	
	public static final String AT_ASTA_SE_FIRSTSCAN = "at-asta-se-firstscan";
	
	public static final String PROD_ASTA_SE_FIRSTSCAN = "prod-asta-se-firstscan";
	
	//public static final String OUTPUT_TOPIC = "OUTPUT_TOPIC";
	
	public static final String Z = "Z";
	
	public static final String PLUS = "+";
	
	public static final String T = "T";

}
