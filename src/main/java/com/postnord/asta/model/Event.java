package com.postnord.asta.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "event_time", "source", "pn_item_id", "product_code", "classification", "dia_flag", "ocr_flag",
		"eu_flag" })
public class Event implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("event_time")
	private String eventTime;
	@JsonProperty("source")
	private String source;
	@JsonProperty("pn_item_id")
	private String pnItemId;
	@JsonProperty("product_code")
	private String productCode;
	@JsonProperty("classification")
	private String classification;
	@JsonProperty("dia_flag")
	private String diaFlag;
	@JsonProperty("ocr_flag")
	private String ocrFlag;
	@JsonProperty("eu_flag")
	private String euFlag;

	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("event_time")
	public String geteventTime() {
		return eventTime;
	}

	@JsonProperty("event_time")
	public void seteventTime(String eventTime) {
		this.eventTime = eventTime;
	}

	@JsonProperty("source")
	public String getSource() {
		return source;
	}

	@JsonProperty("source")
	public void setSource(String source) {
		this.source = source;
	}

	@JsonProperty("pn_item_id")
	public String getPnItemId() {
		return pnItemId;
	}

	@JsonProperty("pn_item_id")
	public void setPnItemId(String pnItemId) {
		this.pnItemId = pnItemId;
	}

	@JsonProperty("product_code")
	public String getProductCode() {
		return productCode;
	}

	@JsonProperty("product_code")
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	@JsonProperty("classification")
	public String getClassification() {
		return classification;
	}

	@JsonProperty("classification")
	public void setClassification(String classification) {
		this.classification = classification;
	}

	@JsonProperty("dia_flag")
	public String getDiaFlag() {
		return diaFlag;
	}

	@JsonProperty("dia_flag")
	public void setDiaFlag(String diaFlag) {
		this.diaFlag = diaFlag;
	}

	@JsonProperty("ocr_flag")
	public String getOcrFlag() {
		return ocrFlag;
	}

	@JsonProperty("ocr_flag")
	public void setOcrFlag(String ocrFlag) {
		this.ocrFlag = ocrFlag;
	}

	@JsonProperty("eu_flag")
	public String getEuFlag() {
		return euFlag;
	}

	@JsonProperty("eu_flag")
	public void setEuFlag(String euFlag) {
		this.euFlag = euFlag;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
