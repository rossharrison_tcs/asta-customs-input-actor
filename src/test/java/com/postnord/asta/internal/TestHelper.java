package com.postnord.asta.internal;

import com.postnord.asta.model.Event;
import com.postnord.potapi.model.util.JsonUtil;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class TestHelper {

    public static InputStream getFileAsStream(String resource) throws IOException {
        File initialFile = new File("src/test/resources/" + resource);
        return new FileInputStream(initialFile);
    }

    private static String getStreamSample(String fileName) throws IOException {

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(getFileAsStream(fileName), StandardCharsets.UTF_8))) {
            return bufferedReader.lines().collect(Collectors.joining());
        }
    }

    public static Event getEvent(String fileName) throws IOException{

        JsonUtil<Event> utils = new JsonUtil<>();
        Event event = utils.fromJson(getStreamSample(fileName), Event.class);
        return event;
    }
}
