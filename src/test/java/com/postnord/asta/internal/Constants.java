package com.postnord.asta.internal;

public class Constants {

    public static final String PROD_ASTA_SE_EVENT = "prod-asta-se-event";
    public static final String PROD_ASTA_SE_FIRSTSCAN = "prod-asta-se-firstscan";
    public static final String POTAPI_TRANSACTION_READ = "potapi-transaction-read";

}
