package com.postnord.asta;

import com.postnord.asta.internal.Constants;
import com.postnord.asta.internal.EventSerde;
import com.postnord.asta.internal.TestHelper;
import com.postnord.asta.internal.TransactionSerde;
import com.postnord.asta.model.Event;
import com.postnord.asta.processor.Processor;
import com.postnord.asta.util.PotapiTransactionSerde;
import com.postnord.orm.commons.configuration.StandardTopic;
import com.postnord.potapi.model.Transaction;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.processor.WallclockTimestampExtractor;
import org.apache.kafka.streams.test.ConsumerRecordFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Properties;

import static org.assertj.core.api.Assertions.assertThat;

public class ProcessorTest {

    private TopologyTestDriver topologyTestDriver;

    @Before
    public void setUp() {
        Properties props = new Properties();
        Processor processor = new Processor();
        props.put(StreamsConfig.CLIENT_ID_CONFIG, "potapi-input-test-client");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "potapi");
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "potapi-input-test");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(StreamsConfig.REPLICATION_FACTOR_CONFIG, 1);
        props.put(StreamsConfig.DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG, WallclockTimestampExtractor.class);
        Topology topology = processor.topology();
        topologyTestDriver = new TopologyTestDriver(topology, props);
    }

    @Test
    public void shouldWriteToTwoTopic() throws Exception{

        Event event = TestHelper.getEvent("asta-se-event.json");
        assertThat(event).isNotNull();
        writeEventToTopology(event);

        ProducerRecord<String, Event> fScans = readFirstScan();
        Event fs = fScans.value();
        assertThat(fs).isNotNull();

        ProducerRecord<String, Transaction> transactions = readTransaction();
        Transaction tx = transactions.value();
        assertThat(tx).isNotNull();
        assertThat(tx.getRetries().getRetries()).isEqualTo(0);
        assertThat(tx.getAction().getAction()).isEqualTo("write");
        assertThat(tx.getActor().getActor()).isEqualTo("asta");
        assertThat(tx.getTransactionId().getTransactionId()).isNotNull();
        assertThat(tx.getEventTime().getEventTime()).isNotNull();
        assertThat(tx.getPayload().getItems().get(0).getPnItemId().getPnItemId()).isEqualTo(event.getPnItemId());
        assertThat(tx.getPayload().getItems().get(0).getItemId().getItemId()).isNotNull();

    }

    @Test
    public void ShouldPopulatePnImportCategory() throws Exception{
        Event event = TestHelper.getEvent("asta-se-event.json");
        assertThat(event).isNotNull();
        writeEventToTopology(event);

        ProducerRecord<String, Transaction> transactions = readTransaction();
        Transaction tx = transactions.value();
        assertThat(tx).isNotNull();

        if(event.getEuFlag().equalsIgnoreCase("1")){
            assertThat(tx.getPayload().getItems().get(0).getCustoms()
                    .getOriginals().get(0).getPnImportCategory().getPnImportCategory())
                    .isEqualTo("EU");
        }else{
            assertThat(tx.getPayload().getItems().get(0).getCustoms()
                    .getOriginals().get(0).getPnImportCategory().getPnImportCategory())
                    .isEqualTo("NON-EU");
        }


    }

    private ProducerRecord<String, Event> readFirstScan() {
        ProducerRecord<String, Event> result = topologyTestDriver.readOutput(
                Constants.PROD_ASTA_SE_FIRSTSCAN, Serdes.String().deserializer(),
                new EventSerde().deserializer());
        if (result == null) {
            return null;
        }
        return result;
    }

    private ProducerRecord<String, Transaction> readTransaction() {
        ProducerRecord<String, Transaction> result = topologyTestDriver.readOutput(
                Constants.POTAPI_TRANSACTION_READ, Serdes.String().deserializer(),
                new TransactionSerde().deserializer());
        if (result == null) {
            return null;
        }
        return result;
    }

    private void writeEventToTopology(Event event) {

        ConsumerRecordFactory<String, Event> factory = new ConsumerRecordFactory<>(
                Serdes.String().serializer(), new EventSerde().serializer());
        topologyTestDriver.pipeInput(factory.create(Constants.PROD_ASTA_SE_EVENT, event.getPnItemId(), event));
    }

    @After
    public void cleanUp(){
        topologyTestDriver.close();
    }
}
